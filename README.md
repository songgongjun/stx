# stx-ansible-deploy
You should set up the network manually before running ansible.

The file default.yml is the configuration file and filepath is the file path of default.yml. The default.yml contains DNS server, docker registry, oam address. Remember to change the oam address to your address.

```
rm $(cat filepath)
mv default_simplex_0708.yml default.yml
cp default.yml $(cat filepath)
```

Replace the $custom-wrsroot-password with your password.  

```
ansible-playbook /usr/share/ansible/stx-ansible/playbooks/bootstrap/bootstrap.yml -e "ansible_become_pass=$custom-wrsroot-password admin_password=$custom-admin-password"  
```

e.g.  

```
ansible-playbook /usr/share/ansible/stx-ansible/playbooks/bootstrap/bootstrap.yml -e "ansible_become_pass=C@ster123 admin_password=C@ster123"  
```

The step "Extend cgts-vg (config_controller method only)" can't be taken when you use ansible to deploy starlingx.
